<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Course;
use Redirect;

class CategoryController extends Controller
{

  private $categoryModel;

    public function __constuct(Category $category){
        $this->categoryModel = $category;
    }

  public function index($slug){
  	$category = Category::with('courses')->where('slug', $slug)->first();

     $category = $this->categoryModel->list('nome', 'id');

  	return view('iegm.site.curso')->with(compact('category'));
  }

  public function curso($id){
    $course = Course::with('category')->where('id', $id)->first();

    return view('iegm.site.teste2')->with(compact('course'));
  }

  public function create(){
  	$category = Category::all();
  	return view('admin.category')->with(compact('category'));

  }

  public function store(Request $request){
    $category = Category::first();
  	$data = $request->toArray();
		$data['slug'] = str_slug($request->name, '-');

		$category->create($data);

		return Redirect::back()->with('success', 'Categoria adicionada com sucesso!');
  }

  public function destroy($id){
     $category = Category::find($id);
     $category->delete();

     return redirect()->route('category.create')->with('success', 'Categoria deletada com sucesso!');
  }
}
