<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Category;
use App\Course;
use App\Redirect;

class CourseController extends Controller
{   
    private $categoryModel;

    public function __constuct(Category $category){
        $this->categoryModel = $category;
    }

    public function index(){
        $category = $this->categoryModel->list('nome', 'id');

        return view('iegm.site.curso')->with(compact('category'));
    }

    public function getCategory($category_id){
        $category = $this->categoryModel->find($category_id);
        $courses = $category->courses()->getQuery->get(['id', 'name']);
        return Response::json($courses);
    }

    public function create(){
    	$categories = Category::all();
    	$categories = $categories->pluck('name', 'id');

    	return view('admin.course')->with(compact('categories'));
    }

    public function store(Request $request){
    	$data = $request->toArray();
    	$course = Course::create($data);
    	return redirect()->route('course.create');
    }

    public function destroy($id){
     $category = Category::find($id);
     $category->delete();

     return redirect()->route('course.create')->with('success', 'Curso deletado com sucesso!');
  }
}
