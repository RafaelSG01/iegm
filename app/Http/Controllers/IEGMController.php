<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Setting;
use App\Inform;
use Redirect;

class IEGMController extends Controller
{
  public function index()
    {
      $setting = Setting::first();

	    return view('index')->with(compact('setting'))->with(compact('inform'));
    }

  public function about()
    {
      return view('iegm.site.instituicao');
    }

  public function createSlide(){

      $setting = Setting::first();

    return view('admin.slide')->with(compact('setting'));
  }

  public function settingSlide(){

      $setting = Setting::first();

      if ($setting == null) {
          $setting = new Setting();

          return view('admin.slide')->with(compact('setting'));
      }      

    return view('admin.slide')->with(compact('setting'));
  }

  public function slideSubmit(Request $request)
    {
        // verifica se o usuario enviou um arquivo

          // if ($request->hasFile('slide1'))
          $config = Setting::first();

          $slide1 = $request->file('slide1');
          $slide2 = $request->file('slide2');
          $slide3 = $request->file('slide3');
          
          $data = $request->toArray();

          if ($request->hasfile('slide1')){
            $slide1_filename = md5(date('slide1 m/d/Y h:i:s a', time())).".".$slide1->getClientOriginalExtension();
            $slide1->move(public_path('images'), $slide1_filename);
            $data['slide1'] = $slide1_filename;
          }

          if ($request->hasfile('slide2')){
            $slide2_filename = md5(date('slide2 m/d/Y h:i:s a', time())).".".$slide2->getClientOriginalExtension();
            $slide2->move(public_path('images'), $slide2_filename);
            $data['slide2'] = $slide2_filename;
          }

          if ($request->hasfile('slide3')){
            $slide3_filename = md5(date('slide3 m/d/Y h:i:s a', time())).".".$slide3->getClientOriginalExtension();
            $slide3->move(public_path('images'), $slide3_filename);
            $data['slide3'] = $slide3_filename;
          }

          if ($config != null) {
            $config->update($data);
          } else {
            $config = Setting::create($data);
          }

          

      return Redirect::back()->with('success', 'Modificações salvas com sucesso!');
    }


  public function adminIndex(){
    $inform = Inform::first();

    return view('admin.iegm')->with(compact('inform'));
  }

  public function settingIndex(){
    $inform = Inform::first();

    if ($inform == null) {
          $inform = new Inform();

          return view('admin.iegm')->with(compact('inform'));
      }

    return view('admin.iegm')->with(compact('inform'));
  }

  public function createIndex(Request $request){
    $admin = Inform::first();

    $logo = $request->file('logo_iegm');

    $iegm = $request->toArray();

    if ($request->hasfile('logo_iegm')){
      $logo_filename = md5(date('logo_iegm m/d/Y h:i:s a', time())).".".$logo->getClientOriginalExtension();
      $logo->move(public_path('images'), $logo_filename);
      $iegm['logo_iegm'] = $logo_filename;
    }

    if ($admin != null) {
      $admin->update($iegm);
    } else {
      $admin = Inform::create($iegm);
    }

    return Redirect::back()->with('success', 'Modificações salvas com sucesso!');
  }

}
