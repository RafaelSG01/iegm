<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Communication;
use Redirect;
use Illuminate\Support\Facades\DB;

class NewsController extends Controller
{

  public function index(){
    $communications = Communication::orderBy('created_at', 'DESC')->paginate(10);
   // $communications = Communication::take(10)->get();

  	return view('iegm.site.imprensa')->with(compact('communications'));
  }

  public function create(){
  	return view('admin.notice');
  }

  public function save(Request $request) {
    $config = Communication::first();

    $news = $request->file('photo_news');

    $data = $request->toArray();

    if($request->hasfile('photo_news')){
      $news_filename = md5(date('photo_news m/d/Y h:i:s a', time())).".".$news->getClientOriginalExtension();
      $news->move(public_path('images'), $news_filename);
      $data['photo_news'] = $news_filename;
    }

    /*if ($config != null) {
      $config->update($data);
    } else {
      $config = Communication::create($data);
    }*/
    $communication = Communication::create($data);


    return Redirect::back()->with('success', 'Modificações salvas com sucesso!');
  }

  public function showNews($slug){
  	$communications = Communication::all();

    $communication = Communication::where('slug', $slug)->first();

    if (is_null($communication)) {
    	return 'Esse evento não existe!';
    }
  	return view('iegm.site.news')->with(compact('communication', 'communications'));
  }
  
	public function list()
	{
	  $communications = Communication::orderBy('created_at', 'DESC')->paginate(10);

	  return view('admin.list-news')->with(compact('communications'));
	}

  public function editView($slug){

    $communications = Communication::where('slug', $slug)->firstOrFail();


    return view('admin.editnotice')->with(compact('communications'));
  }

  public function editNews(Request $request){

    $news = $request->file('photo_news');

    $data = $request->toArray();

    if($request->hasfile('photo_news')){
      $news_filename = md5(date('photo_news m/d/Y h:i:s a', time())).".".$news->getClientOriginalExtension();
      $news->move(public_path('images'), $news_filename);
      $data['photo_news'] = $news_filename;
    }


    if($edit == null){
      return view('admin.notice');
    }else {
      $edit->update($data);
    }
    return view('admin.editnotice')->with(compact('communications'));
  }
}
