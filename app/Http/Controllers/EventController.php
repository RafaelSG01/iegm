<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Event;
use App\Program;
use Redirect;

class EventController extends Controller
{
  public function index(){
  	return view('event.view');
  }

  public function aboutEvent(){
  	return view('event.view');
  }

  public function programEvent(){
  	return view('event.view');
  }

  public function programList(){
    $program = Program::first();
    return view('event.listprogram')->with(compact('program'));
  }

  public function programShow($slug){
    $event = Event::where('slug', $slug)->firstOrFail();
    $program = Program::where()->first();
    if ($program == null) {
        $program = new Program();   

        return view('event.program')->with(compact('event'));
    }


    return view('event.program')->with(compact('event'));
  }

  public function programCreate(Request $request){
    $event = Event::where('slug', $slug)->firstOrFail();
    $program = Program::first();
    $data = $request->toArray();
    $program = Program::create($data);

    return view('event.program')->with('success', 'Programação salva com sucesso!');
  }

  public function programEditShow($slug){
    $program = Program::where('slug', $slug)->firstOrFail();

    return view('event.program')->with('success', 'Programação salva com sucesso!');
  }

  public function programEdit(Request $request){
    $data = $request->toArray();

    if($edit == null){
      return view('event.program');
    }else {
      $edit->update($data);
    }

    return view('event.listprogram')->with('success', 'Programação editada com sucesso!');
  }

  public function programDestroy($id){
     $program = Program::find($id);
     $program->delete();

     return redirect()->route('event.listprogram')->with('success', 'Programação deletada com sucesso!');
  }

  public function credenceEvent(){
  	return view('event.view');
  }


  public function create(){
  	$event = Event::first();

    if ($event == null) {
          $event = new Event();

          return view('event.create')->with(compact('event'));
      }

    return view('event.create')->with(compact('event'));
  }

  public function store(Request $request){
  	$admin = Event::first();

    $logo = $request->file('image');

    $data = $request->toArray();
    $data['slug'] = str_slug($request->name, '-');

    if ($request->hasfile('image')){
      $logo_filename = md5(date('image m/d/Y h:i:s a', time())).".".$logo->getClientOriginalExtension();
      $logo->move(public_path('images'), $logo_filename);
      $data['image'] = $logo_filename;
    }

    if ($admin != null) {
      $admin->update($data);
    } else {
      $admin = Event::create($data);
    }

    return redirect()-route('program.store')->with(compact('program'), 'success', 'Evento criado com sucesso!');
  }

}
