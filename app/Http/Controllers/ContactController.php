<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactFormRequest;

class ContactController extends Controller
{
  public function index(){
  	return view('iegm.site.contato');
  }

  public function store(ContactFormRequest $request){


    $captcha_data = $_POST['g-recaptcha-response'];

    /*if ($captcha_data == null) {
      return \Redirect::route('contato')->with('inform', 'Campo de verificação não selecionado!');
    }*/

    $curl = curl_init();
    curl_setopt_array($curl, [
      CURLOPT_RETURNTRANSFER => true,
      CURLOPT_POST => true,
      CURLOPT_URL => 'https://www.google.com/recaptcha/api/siteverify',
      CURLOPT_POSTFIELDS => [
        'secret' => '6LcHmSkUAAAAAGOlFH5uMHscaX3yt4o5J-Ki9Ue3',
        'response' => $_POST['g-recaptcha-response'],
        'remoteip' => $_SERVER['REMOTE_ADDR']
      ]
    ]);

    $answer = json_decode(curl_exec($curl));
    curl_close($curl);

    if(!$answer->success){
      return \Redirect::route('contato')->with('inform', 'Não foi possível enviar sua mensagem!');
    }else {

      \Mail::send('iegm.site.email',
        array(
            'name' => $request->get('name'),
            'email' => $request->get('email'),
            'user_message' => $request->get('message')
        ), function($message)
        {
            $message->from('rafaelsoares01@gmail.com');
            $message->to('rafaelsoares01@gmail.com', 'Admin')->subject('TODOParrot Feedback');
        });


      return \Redirect::route('contato')->with('message', 'Obrigado por entrar em contato!');
    }


    

  	
  }
}
