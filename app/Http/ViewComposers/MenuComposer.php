<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
// use App\Event;
use App\Category;

class MenuComposer
{
    
    protected $categories;
    // protected $events;

    public function __construct(Category $categories)
    {
        $this->categories = $categories->all();
        // $this->events = $events;
    }

    public function compose(View $view)
    {
        $view->with('categoriesLabel', $this->categories);
        // $view->with('events', $this->events);
    }
}