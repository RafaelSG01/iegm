<?php

namespace App\Http\ViewComposers;

use Illuminate\View\View;
// use App\Event;
use App\Inform;

class InformComposer
{
    
    protected $informs;
    // protected $events;

    public function __construct(Inform $informs)
    {
        $this->informs = $informs->all();
        // $this->events = $events;
    }

    public function compose(View $view)
    {
        $view->with('informsLabel', $this->informs);
        // $view->with('events', $this->events);
    }
}