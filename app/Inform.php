<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Inform extends Model
{
  protected $fillable = [
	 'logo_iegm', 'facebook', 'instagram', 'email', 'phone'];
}
