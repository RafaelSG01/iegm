<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Program extends Model
{
	protected $fillable = [
		'event_id',
		'lecture',
    'speaker',
    'hour',
    'day',
    'inform'
  ];

  public function category()
	{
		return $this->belongsTo(Event::class);
	}
}
