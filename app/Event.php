<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Event extends Model
{
  protected $fillable = ['name', 'slug', 'image', 'description', 'credence'];

  public function courses()
	{
		return $this->hasMany(Program::class);
	}
}
