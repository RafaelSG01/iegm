<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Communication extends Model
{
  protected $fillable = [
      'news_text', 'photo_news', 'title_news', 'slug'];
}
