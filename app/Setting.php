<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Setting extends Model
{
  protected $fillable = [
      'slide1', 'slide2', 'slide3', 'text1', 'text2', 'text3','link1', 'link2', 'link3'
    ];
}
