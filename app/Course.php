<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Course extends Model
{
    
  protected $fillable = [
  	'category_id',
		'name',
		'teacher',
		'date',
		'description',
		'curriculum',
		'value'
  ];

	public function category()
	{
		return $this->belongsTo(Category::class);
	}

}