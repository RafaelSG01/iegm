<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'IEGMController@index']);
Route::get('/instituicao', ['as' => 'instituicao', 'uses' => 'IEGMController@about']);
Route::get('/contato', ['as' => 'contato', 'uses' => 'ContactController@index']);
Route::post('/contato', ['as' => 'contato', 'uses' => 'ContactController@store']);
Route::get('/evento', ['as' => 'evento', 'uses' => 'EventController@index']);
Route::get('/imprensa', ['as' => 'imprensa', 'uses' => 'NewsController@index']);
Route::get('/imprensa/{slug}', ['as' => 'news', 'uses' => 'NewsController@showNews']);
Route::get('/lista-categoria/{slug}', ['as' => 'curso.index', 'uses' => 'CategoryController@index']);
Route::get('get-cursos/{category_id}', 'CourseController@getCourse');
Route::get('/cursos/{id}', ['as' => 'curso.view', 'uses' => 'CategoryController@curso']);



Auth::routes();

Route::group(['middleware' => 'auth'], function () {
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/admin-slide', ['as' => 'slide', 'uses' => 'IEGMController@settingSlide']);
	Route::post('/admin-slide', ['as' => 'slide.submit', 'uses' => 'IEGMController@slideSubmit']);
	Route::get('/admin-pagina-inicial', ['as' => 'inicial', 'uses' => 'IEGMController@settingIndex']);
	Route::post('/admin-pagina-inicial', ['as' => 'inicial.create', 'uses' => 'IEGMController@createIndex']);
	Route::get('/criar-noticia', ['as' => 'notice', 'uses' => 'NewsController@create']);
	Route::post('/criar-noticia', ['as' => 'notice.submit', 'uses' => 'NewsController@save']);
	Route::get('/listar-noticia', ['as' => 'list-news', 'uses' => 'NewsController@list']);
	Route::get('/edit/{slug}', ['as' => 'editnotice', 'uses' => 'NewsController@editView']);
	Route::post('/edit/{slug}', ['as' => 'editnotice.submit', 'uses' => 'NewsController@editNews']);
	Route::get('/categoria/nova', ['as' => 'category.create', 'uses' => 'CategoryController@create']);
	Route::post('/categoria/nova', ['as' => 'category.store', 'uses' => 'CategoryController@store']);
	Route::get('/categoria/nova/{id}', ['as' => 'category.destroy', 'uses' => 'CategoryController@destroy']);
	Route::get('/curso/nova', ['as' => 'course.create', 'uses' => 'CourseController@create']);
	Route::post('/curso/nova', ['as' => 'course.store', 'uses' => 'CourseController@store']);
	Route::get('/admin-evento', ['as' => 'event.create', 'uses' => 'EventController@create']);
	Route::post('/admin-evento', ['as' => 'event.store', 'uses' => 'EventController@store']);
	Route::get('/admin-programacao/{slug}/adicionar', ['as' => 'program.create', 'uses' => 'EventController@programShow']);
	Route::post('/admin-programacao/{slug}/adicionar', ['as' => 'program.store', 'uses' => 'EventController@programCreate']);
	Route::get('/lista-de-programacao', ['as' => 'program.list', 'uses' => 'EventController@programList']);
	Route::get('/admin-programacao/{slug}/edit', ['as' => 'program.show', 'uses' => 'EventController@programEditShow']);
	Route::post('/admin-programacao/{slug}/edit', ['as' => 'program.edit', 'uses' => 'EventController@programEdit']);
	Route::get('/admin-programacao/{slug}/edit', ['as' => 'program.destroy', 'uses' => 'EventController@programDestroy']);
});