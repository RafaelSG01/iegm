var $current, $next, $slides = $(".slideshow .iegm-slide");

function doSlideShow () {
  $current = $slides.filter(".iegm-slide.current");
  $next = $current.next(".iegm-slide");
  if ($next.length < 1) {
    $next = $slides.first();
  }
  $slides.removeClass("previous");
  $current.addClass("previous").removeClass("current");
  $next.addClass("current");
  window.setTimeout(doSlideShow, 7000);
}
window.setTimeout(doSlideShow, 7000);

// Files
$('#slide1, #slide2, #slide3, #logo').change(function() {
  if (this.files[0].size > 3145728) {
    alert("Você só pode enviar arquivos de até 3 megabytes!");
    this.value = '';
    return;
  }
});