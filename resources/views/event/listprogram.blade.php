@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
    <div class="col-md-10 col-md-offset-1">
      @foreach($programs as $program)
        <tr>
          <td>{{ $program->lectre }}</td>
          <td><button class="btn btn-primary"><a href="#">Editar</a></button></td>
          <td class="text-center">
            {{ Form::open(['method' => 'delete', 'route' => ['program.destroy', $program->nome]]) }}
              {{ Form::submit('Deletar', ['class' => 'btn btn-danger']) }}
            {{ Form::close() }}
          </td>
        </tr>
      @endforeach
    </div>
</div>
</div>

<br>
@endsection