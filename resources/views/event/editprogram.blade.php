@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
    <div class="col-md-12">

        <h1>Programação do evento</h1>
        <hr>
        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="col-md-10 col-md-offset-1">          
         {!! Form::open(['route' => ['program.store']]) !!}
            <div class="form-group">
              {!! Form::label('lecture', 'Nome do Evento', array('class' => 'control-label')) !!}
              {!! Form::text('lecture', $event->lecture, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('speaker', 'Descrição do Evento', array('class' => 'control-label')) !!}
              {!! Form::text('speaker', $event->speaker, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('hour', 'Nome do Evento', array('class' => 'control-label')) !!}
              {!! Form::text('hour', $event->hour, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('day', 'Nome do Evento', array('class' => 'control-label')) !!}
              {!! Form::date('day', $event->day, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('inform', 'Credenciamento', array('class' => 'control-label')) !!}
              {!! Form::textarea('inform', $event->inform, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
            </div>
            {!! Form::submit('Próxima Etapa', ['class' => 'btn btn-primary']); !!}
          {!! Form::close() !!}
      </div>
    </div>
    </div>
</div>
</div>

<br>
@endsection