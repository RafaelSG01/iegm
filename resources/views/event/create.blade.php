@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
    <div class="col-md-12">

        <h1>Crie seu evento</h1>
        <hr>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Modi fugiat eveniet soluta enim rerum autem commodi in magni ex, quia dignissimos at sunt facere, mollitia harum eligendi consectetur. Necessitatibus, modi.</p>

        @if($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach($errors->all() as $error)
                    <li>{{$error}}</li>
                @endforeach
            </ul>
        </div>
        @endif

        <div class="col-md-10 col-md-offset-1">
        {!! Form::open(['route' => ['event.store']]) !!}
          <div class="form-group">
              {!! Form::label('image', 'Imagem', array('class' => 'control-label')) !!}
              {!! Form::file('image', null,  ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('name', 'Nome do Evento', array('class' => 'control-label')) !!}
              {!! Form::text('name', $event->name, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('description', 'Descrição do Evento', array('class' => 'control-label')) !!}
              {!! Form::textarea('description', $event->description, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('credence', 'Credenciamento', array('class' => 'control-label')) !!}
              {!! Form::textarea('credence', $event->credence, ['class' => 'form-control', 'id' => 'artigo-ckeditor2']) !!}
            </div>
            {!! Form::submit('Próxima Etapa', ['class' => 'btn btn-primary']); !!}
        {!! Form::close() !!}
      </div>
    </div>
    </div>
</div>
</div>

<br>
@endsection