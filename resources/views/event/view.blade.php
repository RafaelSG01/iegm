@extends('iegm.app')

@section('content')

<div class="container">
<div class="row">
    <div class="col-md-12">
        <h1>Eventos cadastrados</h1>
        <hr>
        
            <table class="table table-bordered">
                <tr>
                    <td><b>Evento</b></td>
                    <td><b>Palestrante</b></td>
                    <td><b>Preço</b></td>
                    <td><b>Visualizar</b></td>
                </tr>
                @foreach($events as $event)
                <tr>
                    <td>{{$event->name}}</td>
                    <td>{{$event->speaker}}</td>
                    <td>{{$event->price}}</td>
                    <td><a href="{{ route('event.view', ['slug' => $event->slug]) }}">Visualizar</a></td>
                </tr>
                @endforeach
            </table>
    </div>
</div>
</div>

<br>
@endsection