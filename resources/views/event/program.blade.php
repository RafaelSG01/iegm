@extends('layouts.app')

@section('content')

<div class="container">
<div class="row">
    <div class="col-md-12">

        <h1>Programação do evento</h1>
        <hr>
          @if (\Session::has('success'))
            <div class="alert alert-success">
              <ul>
                  <li>{!! \Session::get('success') !!}</li>
              </ul>
            </div>
          @endif

        <div class="col-md-10 col-md-offset-1">          
         {!! Form::open(['route' => ['program.store']]) !!}
            {!! Form::hidden('event_id', $event->id) !!}
            <div class="form-group">
              {!! Form::label('lecture', 'Programa', array('class' => 'control-label')) !!}
              {!! Form::text('lecture', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('speaker', 'Palestrante', array('class' => 'control-label')) !!}
              {!! Form::text('speaker', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('hour', 'Horário', array('class' => 'control-label')) !!}
              {!! Form::text('hour', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('day', 'Data', array('class' => 'control-label')) !!}
              {!! Form::date('day', ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('inform', 'Informação', array('class' => 'control-label')) !!}
              {!! Form::textarea('inform', ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
            </div>
            {!! Form::submit('Criar', ['class' => 'btn btn-primary']); !!}
          {!! Form::close() !!}
      </div>
    </div>
    </div>
</div>

</div>

<br>
@endsection