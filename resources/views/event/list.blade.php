@extends('event.app')

@section('content')

<div class="container">
<div class="row">
    <div class="col-md-12">
        <img src="{{$event->year}}" class="img-responsive">
        <h1>Evento {{$event->name}}</h1>
        <hr>
        <h2 id="valor">2. Valor</h2>
        <p>{{$event->price}}</p>
        <h2 id="palestrante">3. Palestrante</h2>
        <p>{{$event->speaker}}</p>
        <h2 id="descricao">1. Descricao</h2>
        <p>{{$event->description}}</p>

    </div>
</div>
</div>

<br>
@endsection