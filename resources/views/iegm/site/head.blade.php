<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>IEGM - Instituto de Educaçã Gomes de Melo</title>
	<link rel="stylesheet" href="{{asset('css/bootstrap.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/camera.css')}}">
	<link rel="stylesheet" href="{{asset('css/font-awesome.min.css')}}">
	<link rel="stylesheet" href="{{asset('css/style.css')}}">
	<script src='https://www.google.com/recaptcha/api.js? Onload = onloadCallback & render = explicit' async defer></script>
</head>
<body>

	<div class="conteudo">
	<div class="color-menu">
		<div class="container">
			<div class="row">
				<div class="col-md-12 up-iegm">
					<ul class="list-inline access-user pull-right">
						<li><i class="fa fa-user-circle-o" aria-hidden="true"></i> Docentes</li>
						<li><i class="fa fa-user-circle-o" aria-hidden="true"></i> Alunos</li>
					</ul>
				</div>
			</div>
			<div class="row">
				<div class="col-md-12 up-iegm">
					<hr class="hr-iegm">
				</div>
			</div>
		</div>
	</div>	
	<nav class="navbar navbar-default color-menu">
	  <div class="container">
	    <!-- Brand and toggle get grouped for better mobile display -->
	    <div class="navbar-header">
	      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#bs-example-navbar-collapse-1" aria-expanded="false">
	        <span class="sr-only">Toggle navigation</span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	        <span class="icon-bar"></span>
	      </button>
	      <a href="{{route('index')}}"><img src="{{ asset('images/logo_iegm.png')}}" class="img-responsive"></a>
	    </div>

	    <!-- Collect the nav links, forms, and other content for toggling -->
	    <div class="collapse navbar-collapse navbar-right" id="bs-example-navbar-collapse-1">
	      <ul class="nav navbar-nav">
	        <li class="box-menu curmudgeon"><a href="{{route('instituicao')}}">Instituição <span class="sr-only">(current)</span></a></li>
	        <li class="box-menu curmudgeon dropdown"><a class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Cursos <span class="caret"></span></a>
	        	<ul class="dropdown-menu">
	            @foreach($categoriesLabel as $categoryLabel)
								<li><a href="{{route('curso.index', ['slug' => $categoryLabel->slug])}}">{{ $categoryLabel->name }}</a></li>
	        		@endforeach
            </ul>
	        </li>
	        <li class="box-menu curmudgeon dropdown"><a lass="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Eventos <span class="caret"></span></a>
	        	<ul class="dropdown-menu">
	            
            </ul>
	        </li>
	        <li class="box-menu curmudgeon"><a href="{{route('imprensa')}}">Imprensa</a></li>
	        <li class="box-menu curmudgeon"><a href="{{route('contato')}}">Contatos</a></li>	        
	      </ul>
	    </div><!-- /.navbar-collapse -->	    
	  </div><!-- /.container-fluid -->
	</nav>

	@yield('content')
</div>
<div class="rodape color-menu">
<!-- footer -->
	<footer>
		<div class="container">
			<div class="row up-iegm">
  		</div>
			<div class="row text-center">
				<div class="col-md-5 up-iegm">
				 	<ul class="list-inline">
						<li><strong>Tel: </strong>(85) 3013-5411</li>
						<li><strong>E-mail: </strong>contato@institutogomesdemelo.com.br</li>
					</ul>
				</div>
				<div class="col-md-2 up-iegm">
					<ul class="list-inline">
						<li><a href="https://www.facebook.com/iegmoficial/"></a><i class="fa fa-facebook-official" aria-hidden="true"></i></li>
						<li><a href="https://www.instagram.com/iegmoficial/"></a><i class="fa fa-instagram" aria-hidden="true"></i></li>
					</ul>
				</div>
				<div class="col-md-5 up-iegm">
					<p><b>@copyright - 2014</b> - Instituto de Educação Gomes de Melo</p>					
				</div>
			</div>
		</div>
	</footer>
</div>


	<script src="{{ asset('js/jquery.min.js')}}"></script>
	<script src="{{ asset('js/jquery.mobile.customized.min.js')}}"></script>
	<script src="{{ asset('js/jquery.easing.1.3.js')}}"></script>
	<script src="{{ asset('js/bootstrap.min.js')}}"></script>
	<script src="{{ asset('js/scripts.js')}}"></script>	
	
</body>
</html>
