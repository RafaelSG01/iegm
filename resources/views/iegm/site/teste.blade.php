@extends('iegm.site.head')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Cursos da categoria: {{$category->name}}</h1>
				<hr>
				@foreach($category->courses as $course)
					<p><b>Nome: </b>{{ $course->name }}</p>
					<p><b>Instrutor:</b> {{ $course->teacher }}</p>
				@endforeach
			</div>
		</div>
	</div>
@endsection