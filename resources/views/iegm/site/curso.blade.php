@extends('iegm.site.head')

@section('content')
		<div class="container">
			<br><br>
  		<div class="row">
  			<div class="col-md-4 col-md-offset-2">  				  				
					<div class="box-iegm">
			    	<h4>{{$category->name}}</h4>
			    </div>
			  </div>
		    <div class="col-sm-4 select-course">
		      <select class="form-control">
					  <option>Selecione o Curso</option>
					  @foreach($category->courses as $course)
							<option>{{ $course->name }}</option>
						@endforeach
					</select>
		    </div>
    	</div>
    </div>
	    <div class="container">
				<div class="row">
					<div class="col-md-12">
						<h2>Titulo curso</h2>
						<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ipsam doloremque officiis impedit est deserunt quis. Dicta eaque cupiditate quae nulla, magnam atque quisquam labore esse, eius non assumenda asperiores quam. 
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Eligendi, error ad aliquid, minima dicta quod esse laudantium. Provident laudantium illum ratione magnam, neque praesentium perspiciatis minima! Voluptatum modi beatae est.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6 col-md-offset-3">
						<h2>Grade curricular</h2>
						<ul class="list-group">
							<li class="list-group-item">informe 1</li>
							<li class="list-group-item">informe 2</li>
							<li class="list-group-item">informe 3</li>
							<li class="list-group-item">informe 4</li>
							<li class="list-group-item">informe 5</li>
							<li class="list-group-item">informe 6</li>
							<li class="list-group-item">informe 7</li>
							<li class="list-group-item">informe 8</li>
							<li class="list-group-item">informe 9</li>
							<li class="list-group-item">informe 10</li>
						</ul>
						<button type="button" class="btn btn-primary">Matricule-se</button>
					</div>
				</div>
  		</div>
			<br><br>
		</div>

@endsection