@extends('iegm.site.head')

@section('content')
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h1>Curso: {{$course->name}}</h1>
				<hr>
				<p><b>Instrutor:</b> {{$course->teacher}}</p>
				<p><b>Categoria:</b> {{$course->category->name}}</p>
			</div>
		</div>
	</div>
@endsection