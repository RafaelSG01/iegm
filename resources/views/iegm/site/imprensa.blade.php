@extends('iegm.site.head')

@section('content')
	<div class="container">
		<br>
		@foreach($communications as $communication)
		<br>
			<div class="row">				
				<div class="col-md-2 col-md-offset-1">
					<div class="thumbnail">
						<img src="{{asset('images/'.$communication->photo_news)}}" class="img-responsive img-circle">
					</div>
				</div>
				<div class="col-md-8">
					<small>Publicado em {{$communication->created_at}}</small>
					<a href="{{ route('news', ['slug' => $communication->slug]) }}"><h3>{{$communication->title_news}}</h3></a>
				</div>
			</div>
		@endforeach
		
	</div>
@endsection