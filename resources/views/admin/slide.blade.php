@extends('layouts.app')

@section('content')
	<div class="container">
		@if (\Session::has('success'))
	    <div class="alert alert-success">
	      <ul>
	          <li>{!! \Session::get('success') !!}</li>
	      </ul>
	    </div>
		@endif
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				{!! Form::open(['route' => 'slide', 'files' => true, 'method' => 'post', 'action' => 'IEGMController@slideSubmit']) !!}
					
			    <div class="form-group">
              {!! Form::label('slide1', 'Slide 1', array('class' => 'control-label')) !!}
              {!! Form::file('slide1', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('text1', 'Descrição', array('class' => 'control-label')) !!}
              {!! Form::textarea('text1', $setting->text1, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('link1', 'Link', array('class' => 'control-label')) !!}
              {!! Form::text('link1', $setting->link1, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('slide2', 'Slide 2', array('class' => 'control-label')) !!}
              {!! Form::file('slide2', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('text2', 'Descrição', array('class' => 'control-label')) !!}
              {!! Form::textarea('text2', $setting->text2, ['class' => 'form-control', 'id' => 'artigo-ckeditor2']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('link2', 'Link', array('class' => 'control-label')) !!}
              {!! Form::text('link2', $setting->link2, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('slide3', 'Slide 3', array('class' => 'control-label')) !!}
              {!! Form::file('slide3', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('text3', 'Descrição', array('class' => 'control-label')) !!}
              {!! Form::textarea('text3', $setting->text3, ['class' => 'form-control', 'id' => 'artigo-ckeditor3']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('link3', 'Link', array('class' => 'control-label')) !!}
              {!! Form::text('link3', $setting->link3, ['class' => 'form-control']) !!}
            </div>

            {!! Form::submit('Salvar', ['class' => 'btn btn-primary']); !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection