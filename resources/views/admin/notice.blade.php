@extends('layouts.app')

@section('content')
<div class="container">
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
      </div>
    @endif
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				{!! Form::open(['route' => 'notice', 'files' => true, 'method' => 'post', 'action' => 'NewsController@save']) !!}
			    <div class="form-group">
              {!! Form::label('photo_news', 'Imagem', array('class' => 'control-label')) !!}
              {!! Form::file('photo_news', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('title_news', 'Título', array('class' => 'control-label')) !!}
              {!! Form::text('title_news', null, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('slug', 'Slug', array('class' => 'control-label')) !!}
              {!! Form::text('slug', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('news_text', 'Notícia', array('class' => 'control-label')) !!}
              {!! Form::textarea('news_text', null, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
            </div>
            {!! Form::submit('Publicar', ['class' => 'btn btn-primary']); !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection