@extends('layouts.app')

@section('content')

<div class="container">
	<div class="row">
		<div class="col-md-12">
			<h1>Notícias</h1>
			<hr>
			@foreach($communications as $communication)
				<ul>
					<li><a href="{{ route('editnotice', ['slug' => $communication->slug]) }}">{{$communication->title_news}}</a></li>
				</ul>
			@endforeach

		</div>
	</div>
</div>

<br>
@endsection