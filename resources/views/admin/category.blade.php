@extends('layouts.app')

@section('content')
	<div class="container">
		@if (\Session::has('success'))
	    <div class="alert alert-success">
	      <ul>
	          <li>{!! \Session::get('success') !!}</li>
	      </ul>
	    </div>
		@endif
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				<table class="table table-bordered">
					<tr>
						<td class="text-center"><b>Categoria</b></td>
						<td class="text-center"><b>Ação</b></td>
					</tr>
				  @foreach($category as $category)
						<tr>
							<td>{{ $category->name }}</td>
							<td class="text-center">
								<a href="{{ route('category.destroy', $category->id) }}" class="btn btn-danger">DELETAR</a>
							</td>
						</tr>
					@endforeach
				</table>
			</div>
		</div>
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				{!! Form::open(['route'=>'category.store']) !!}
    			<div class="form-group">
    				 {!! Form::label('name', 'Adicionar Categoria', array('class' => 'control-label')) !!}
    				 {!! Form::text('name', null,  ['class' => 'form-control']) !!}
    			</div>
    			{!! Form::submit('Adicionar', ['class' => 'btn btn-primary']); !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection