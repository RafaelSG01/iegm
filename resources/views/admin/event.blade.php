@extends('layouts.app')

@section('content')
<div class="container">
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
      </div>
    @endif
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				{!! Form::open(['route' => ['event.store']]) !!}
			    <div class="form-group">
              {!! Form::label('image', 'Imagem', array('class' => 'control-label')) !!}
              {!! Form::file('image', null,  ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('name', 'Título', array('class' => 'control-label')) !!}
              {!! Form::text('name', $events->name, ['class' => 'form-control']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('slug', 'Slug', array('class' => 'control-label')) !!}
              {!! Form::text('slug', $events->slug, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('description', 'Notícia', array('class' => 'control-label')) !!}
              {!! Form::textarea('description', $events->description, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('program', 'Notícia', array('class' => 'control-label')) !!}
              {!! Form::textarea('program', $events->program, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
            </div>
            <div class="form-group">
              {!! Form::label('credence', 'Notícia', array('class' => 'control-label')) !!}
              {!! Form::textarea('credence', $events->credence, ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
            </div>
            {!! Form::submit('Criar', ['class' => 'btn btn-primary']); !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection