@extends('layouts.app')

@section('content')
	<div class="container">
    @if (\Session::has('success'))
      <div class="alert alert-success">
        <ul>
            <li>{!! \Session::get('success') !!}</li>
        </ul>
      </div>
    @endif
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				{!! Form::open(['route' => 'inicial', 'files' => true, 'method' => 'post', 'action' => 'IEGMController@createIndex']) !!}
			    <div class="form-group">
              {!! Form::label('logo_iegm', 'Logo', array('class' => 'control-label')) !!}
              {!! Form::file('logo_iegm', null, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('phone', 'Telefone', array('class' => 'control-label')) !!}
              {!! Form::text('phone', $inform->phone, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('email', 'E-mail', array('class' => 'control-label')) !!}
              {!! Form::text('email', $inform->email, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('facebook', 'Facebook', array('class' => 'control-label')) !!}
              {!! Form::text('facebook', $inform->facebook, ['class' => 'form-control']) !!}
            </div>

            <div class="form-group">
              {!! Form::label('instagram', 'Instagram', array('class' => 'control-label')) !!}
              {!! Form::text('instagram', $inform->instagram, ['class' => 'form-control']) !!}
            </div>

            {!! Form::submit('Criar', ['class' => 'btn btn-primary']); !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection