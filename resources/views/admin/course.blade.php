@extends('layouts.app')

@section('content')

	<div class="container">
		<div class="row">
			<div class="col-md-10 col-md-offset-1">
				{!! Form::open(['route'=>'course.store']) !!}
    			<div class="form-group">
    				 {!! Form::label('name', 'Nome do Curso', array('class' => 'control-label')) !!}
    				 {!! Form::text('name', null,  ['class' => 'form-control']) !!}
    				 {!! Form::label('teacher', 'Palestrante', array('class' => 'control-label')) !!}
    				 {!! Form::text('teacher', null,  ['class' => 'form-control']) !!}
    				 {!! Form::label('date', 'Data do Curso', array('class' => 'control-label')) !!}
    				 {!! Form::date('date', null,  ['class' => 'form-control']) !!}
    				 {!! Form::label('description', 'Descrição', array('class' => 'control-label')) !!}
    				 {!! Form::text('description', ['class' => 'form-control', 'id' => 'artigo-ckeditor1']) !!}
    				 {!! Form::label('curriculum', 'Grade curricular', array('class' => 'control-label')) !!}
    				 {!! Form::text('curriculum', null,  ['class' => 'form-control', 'id' => 'artigo-ckeditor2']) !!}
    				 {!! Form::label('category_id', 'Categoria', array('class' => 'control-label')) !!}
    				 {!! Form::select('category_id', $categories, null, ['placeholder' => 'Escolha a Categoria', 'class'=>'form-control']) !!}
    				 {!! Form::hidden('value', '0') !!}
    			</div>
    			{!! Form::submit('Adicionar', ['class' => 'btn btn-primary']); !!}
				{!! Form::close() !!}
			</div>
		</div>
	</div>
@endsection